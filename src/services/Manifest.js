import MisteryIconPokemon from '../assets/images/mewtwo@2x.png';
import MisteryImagePokemon from '../assets/images/big-mewtwo-mistery.png';

export const ManifestTypes = [
  'bug',
  'dark',
  'dragon',
  'electric',
  'fairy',
  'fight',
  'flying',
  'ghost',
  'grass',
  'ground',
  'ice',
  'normal',
  'poison',
  'psychic',
  'rock',
  'steel',
  'water'
];
export let creatingPokemon = {
  english_name: 'Name Of Pokemon',
  experience: '0000000',
  type: [],
  image_url: MisteryImagePokemon,
  icon_url: MisteryIconPokemon,
  attack: '0',
  defense: '0',
  speed: '0',
  abilities: [],
  weight_kg: '0',
  height_m: '0',
  percentage_male: '0',
  percentage_female: '0',
  generation: '0'
}
export const ManifestAttack = [];
export const ManifestExperience = [];