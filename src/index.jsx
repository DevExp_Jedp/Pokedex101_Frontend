import React from "react";
import ReactDOM from "react-dom";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import { createUploadLink } from "apollo-upload-client";

// Styles
import "./styles/main.sass";

// Components
import App from "./routes/App";

// Context
import { PokemonProviders } from "./providers/PokemonProviders";

// Apollo
const link = createUploadLink({ uri: "https://pokedexapi.guerracode.com" });

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <PokemonProviders>
      <App />
    </PokemonProviders>
  </ApolloProvider>,
  document.getElementById("app")
);
