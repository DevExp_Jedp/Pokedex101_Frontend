import React, {useState, createContext} from 'react'
import { creatingPokemon } from '../services/Manifest';

export const PokemonsContext = createContext();

export const PokemonProviders = ({children}) => {

  const [pokemons, setPokemons] = useState([]);
  const [filteredPokemons, setFilteredPokemons] = useState([]);
  
  const [pokemon, setPokemon] = useState({});

  const [strongest, setStrongest] = useState([]);
  const [filteredStrongest, setFilteredStrongest] = useState([]);

  const [weaknest, setWeaknest] = useState([]);
  const [filteredWeaknest, setFilteredWeaknest] = useState([]);

  const [isLegendaries, setIsLegendaries]  = useState([]);
  const [filteredIsLegendaries, setFilteredIsLegendaries]  = useState([]);

  const [currentPage, setCurrentPage] = useState(1);
  const [currentPageStrongest, setCurrentPageStrongest] = useState(1);
  const [currentPageWeaknest, setCurrentPageWeaknest] = useState(1);
  const [currentPageIslegendaries, setCurrentPageIsLegendaries] = useState(1);

  const [createPokemon, setCreatePokemon] = useState(creatingPokemon);
  const [newCreatedPokemon, setNewCreatedPokemon] = useState(creatingPokemon);


  const store = {
    pokemons: [pokemons, setPokemons],
    filteredPokemons: [filteredPokemons, setFilteredPokemons],
    pokemon: [pokemon, setPokemon],
    strongest: [strongest, setStrongest],
    filteredStrongest: [filteredStrongest, setFilteredStrongest],
    weaknest: [weaknest, setWeaknest],
    filteredWeaknest: [filteredWeaknest, setFilteredWeaknest],
    isLegendaries: [isLegendaries, setIsLegendaries],
    filteredIsLegendaries: [filteredIsLegendaries, setFilteredIsLegendaries],
    currentPage: [currentPage, setCurrentPage],
    currentPageStrongest: [currentPageStrongest, setCurrentPageStrongest],
    currentPageWeaknest: [currentPageWeaknest, setCurrentPageWeaknest],
    currentPageIslegendaries: [currentPageIslegendaries, setCurrentPageIsLegendaries],
    createPokemon: [createPokemon, setCreatePokemon],
    createdPokemon: [newCreatedPokemon, setNewCreatedPokemon]
  }

  return (
    <PokemonsContext.Provider value={store}>
      {children}
    </PokemonsContext.Provider>
  )
}