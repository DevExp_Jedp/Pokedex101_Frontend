import React from 'react';

// Components
import Header from '../components/Header/Header';
import TextArticle from '../components/TextArticle/TextArticle';
import SmallArticle from '../components/SmallArticle/SmallArticle';
import BigArticle from '../components/BigArticle/BigArticle';
import Footer from '../components/Footer/Footer';

// Images
import Mewtwo from '../assets/images/big-mewtwo.png';
import MiniMewtwo from '../assets/images/mewtwo.png';
import LegendPokeball from '../assets/images/Pokeball-legendary.png';
import Pokemons from '../assets/images/img-pokemons.png';

export class Landing extends React.Component {

  render() {
    return (
      <section className="landing">
        <Header />
        <section className="landing__welcome" id="content">
          <div className="landing__welcome-article">
            <TextArticle 
              title="Find Your Pokemon" 
              description="Find your favorite pokemons of all generations and know each detail for become a master pokemon!"
              position="left"
              onClick={() => this.props.history.push('/pokemons')}
            />
          </div>
          <figure className="landing__welcome-pokemons">
            <img src={Pokemons} alt="All the pokemons in one place" aria-label="All the pokemons in one place" />
            <strong className="h4">ALL POKEMON IN ONE PLACE</strong>
          </figure>
        </section>

        <section className="landing__categories">
          <figure className="landing__categories-images">
            <img className="landing__categories-images-mewtwo" src={Mewtwo} alt="Mewtwo, pokemon legendario" aria-label="Mewtwo, pokemon legendario"/>
            <img className="landing__categories-images-mini-mewtwo" src={MiniMewtwo} alt="Mewtwo, pokemon legendario del juego" aria-label="Mewtwo, pokemon legendario del juego" />
            <figcaption className="h4">LEGENDARIES</figcaption>
          </figure>

          <div className="landing__categories-article">
            <TextArticle 
              title="Know Each Detail"
              description="Know who are the strongest one and who are the weakest pokemons of all generations and don't forget find your ledengary for win all the battles."
              positon="left"
              onClick={() => this.props.history.push('/categories')}
            />
          </div>
        </section>

        <article className="landing__advantages">

          <div className="landing__advantages-article">
            <BigArticle 
              title="WHY POKEDEX 101?"
              text="Here you will find all the pokemons that you need also, Pokedex 101 will bring you the best experience to search a Pokemon. Specially with his old school interface that bring us that memories of our chillhood."
              color="#11121C"
              position="center"
            /> 
          </div>
          
          <div className="landing__advantages-articles">
            <SmallArticle 
              title="All Generations"
              text="Search trough all the generation of Pokemon and find your favorite one"
              img={LegendPokeball}
              color="#11121C"
            />
            <SmallArticle 
              title="Categories"
              text="Find since the strongest, weaknest pokmeon to the most strangers and legendaries pokemons. "
              img={LegendPokeball}
              color="#11121C"
            />
            <SmallArticle 
              title="Powerful Pokedex"
              text="Pokedex 101 allows you find the exact pokemon that you want"
              img={LegendPokeball}
              color="#11121C"
            />
          </div>
        </article>

        <Footer />        
      </section>
    )
  }
}

export default Landing;
