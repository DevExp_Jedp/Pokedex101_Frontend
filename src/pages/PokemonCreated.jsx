import React from "react";
// Components
import SuccessfulCreation from "../components/SuccessfulCreation/SuccessfulCreation";
import CreatePokemon from "../components/createPokemon/CreatePokemon";

class PokemonCreated extends React.Component {
  constructor(props) {
    super(props);
    this.goToPokedex = this.goToPokedex.bind(this);
  }

  goToPokedex() {
    this.props.history.push("/");
  }

  render() {
    return (
      <section className="create">
        <div className="create__pokemon">
          <CreatePokemon />
        </div>
        <div className="create__form">
          <SuccessfulCreation onClick={this.goToPokedex} />
        </div>
      </section>
    );
  }
}

export default PokemonCreated;
