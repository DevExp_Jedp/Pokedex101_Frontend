import React, { useState, useContext } from "react";

// Components
import Header from "../components/Header/Header";
import CategoryCard from "../components/CategoryCard/CategoryCard";

// Images
import MiniMewtwo from "../assets/images/mewtwo.png";
import Mewtwo from "../assets/images/big-mewtwo.png";
import Pikachu from "../assets/images/Pikachu.png";
import MiniPikachu from "../assets/images/MiniPikachu.png";
import Squirtle from "../assets/images/Squirtle.png";
import MiniSquirtle from "../assets/images/Squirt{e.png";

// context
import { PokemonsContext } from "../providers/PokemonProviders";

// Apollo
import { useQuery } from "@apollo/client";
import CategoriesPokmeonsGraphql from "../services/CategoriesPokemons.graphql";
import IsLegendaries from "../services/IsLegendaries.graphql";

const Categories = (props) => {
  // Context
  const context = useContext(PokemonsContext);

  const [pokemons, setPokemons] = context.pokemons;
  const [filteredPokemons, setFilteredPokemons] = context.filteredPokemons;
  const [strongest, setStrongest] = context.strongest;
  const [filteredStrongest, setFilteredStrongest] = context.filteredStrongest;
  const [weaknest, setWeaknest] = context.weaknest;
  const [filteredWeaknest, setFilteredWeaknest] = context.filteredWeaknest;
  const [isLegendaries, setIsLegendaries] = context.isLegendaries;
  const [
    filteredIsLegendaries,
    setFilteredIsLegendaries,
  ] = context.filteredIsLegendaries;

  const {
    loading: loading_weaknest,
    error: error_weaknest,
    data: data_weaknest,
  } = useQuery(CategoriesPokmeonsGraphql, {
    variables: {
      force: 0,
      page: 1,
      numberOfPages: 20,
    },
  });

  const {
    loading: loading_strongest,
    error: error_strongest,
    data: data_strongest,
  } = useQuery(CategoriesPokmeonsGraphql, {
    variables: {
      force: 2,
      page: 1,
      numberOfPages: 20,
    },
  });

  const {
    loading: loading_legendaries,
    error: error_legendaries,
    data: data_legendaries,
  } = useQuery(IsLegendaries, {
    variables: {
      type: "psychic",
      page: 1,
      numberOfPages: 20,
    },
  });

  if (data_weaknest && weaknest.length === 0) {
    setWeaknest(data_weaknest.getByForce);
    setFilteredWeaknest(data_weaknest.getByForce);
    setPokemons([...pokemons, ...data_weaknest.getByForce]);
    setFilteredPokemons([...pokemons, ...data_weaknest.getByForce]);
  }

  if (data_strongest && strongest.length === 0) {
    setStrongest(data_strongest.getByForce);
    setFilteredStrongest(data_strongest.getByForce);
    setPokemons([...pokemons, ...data_strongest.getByForce]);
    setFilteredPokemons([...pokemons, ...data_strongest.getByForce]);
  }

  if (data_legendaries && isLegendaries.length === 0) {
    setIsLegendaries(data_legendaries.getLegendary);
    setFilteredIsLegendaries(data_legendaries.getLegendary);
    setPokemons([...pokemons, ...data_legendaries.getLegendary]);
    setFilteredPokemons([...pokemons, ...data_legendaries.getLegendary]);
  }

  return (
    <>
      <Header />
      <section className="categories">
        <CategoryCard
          title="Strongest"
          type="electric"
          pokemons={strongest}
          onClick={() => props.history.push("/pokemons/strongest")}
          pokemon={Pikachu}
          miniPokemon={MiniPikachu}
          description="The Strongest are known for his danger and unique behavior"
          second_description="The Strongest are known for his danger and unique behavior"
        />
        <CategoryCard
          title="Weakest"
          type="water"
          pokemons={weaknest}
          onClick={() => props.history.push("/pokemons/weaknest")}
          pokemon={Squirtle}
          miniPokemon={MiniSquirtle}
          second_description="The Weaknest are most known by his charm and love to his masters"
          description="The Weaknest are most known by his charm and love to his masters"
        />
        <CategoryCard
          title="Can Be Legendaries"
          type="psycho"
          pokemon={Mewtwo}
          miniPokemon={MiniMewtwo}
          pokemons={isLegendaries}
          onClick={() => props.history.push("/pokemons/legendaries")}
          second_description="The Legendaries are the most difficult and strange type to catch"
          description="The Legendaries are the most difficult and strange type to catch"
        />
      </section>
    </>
  );
};

export default Categories;
