import React, {useState, useEffect, useContext} from 'react'


// Apollo
import { useQuery, useLazyQuery } from '@apollo/client';
// GraphQl
import gql from 'graphql-tag';
import PokemonsGraphQl from '../services/pokemonsGraphql.graphql';
import CategoriesPokemonsGraphql from '../services/CategoriesPokemons.graphql';
import PokemonsIsLegendariesGraphql from '../services/IsLegendaries.graphql';

// Components
import Pokedex from '../components/Pokedex/Pokedex';
import Pokemon from '../components/Pokemon/Pokemon';

// Store
import { PokemonsContext } from '../providers/PokemonProviders';


const Pokemons = (props) => {


  // ------------- Context -------------
  // ------------- Context -------------
  const context = useContext(PokemonsContext);
  const [pokemons, setPokemons] = context.pokemons;
  const [filteredPokemons, setFilteredPokemons] = context.filteredPokemons;

  const [strongest, setStrongest] = context.strongest;
  const [filteredStrongest, setFilteredStrongest] = context.filteredStrongest;

  const [weaknest, setWeaknest] = context.weaknest;
  const [filteredWeaknest, setFilteredWeaknest] = context.filteredWeaknest;

  const [isLegendaries, setIsLegendaries] = context.isLegendaries;
  const [filteredIsLegendaries, setFilteredIsLegendaries] = context.filteredIsLegendaries;

  const [currentPage, setCurrentPage] = context.currentPage;
  const [currentPageStrongest, setCurrentPageStrongest] = context.currentPageStrongest;
  const [currentPageWeaknest, setCurrentPageWeaknest] = context.currentPageWeaknest;
  const [currentPageIslegendaries, setCurrentPageIsLegendaries] = context.currentPageIslegendaries;






  // ------------- State -------------
  // ------------- State -------------
  const [responsivePokedex, setResponsivePokedex] = useState(false);
  const [currentPokemon, setCurrentPokemon] = useState({});
  const [lastsPokemons, setLastsPokemons] = useState([]);







  // ------------- Apollo -------------
  // ------------- Apollo -------------
  const {loading, error, data} = useQuery(PokemonsGraphQl, {variables: {page: currentPage} })
  
  const [NextPokemons, {loading: loadingNextPokemons, data: dataNextPokemons}] = useLazyQuery(PokemonsGraphQl);
  const [NextStrongestPokemons, {loading: loadingNextStrongestPokemons, data: dataNextStrongestPokemons}] = useLazyQuery(CategoriesPokemonsGraphql);
  const [NextWeaknestPokemons, {loading: loadingNextWeaknestPokemons, data: dataNextWeaknestPokemons}] = useLazyQuery(CategoriesPokemonsGraphql);
  const [NextLegendariesPokemons, {loading: loadingNextLegendariesPokemons, data: dataNextLegendariesPokemons}] = useLazyQuery(PokemonsIsLegendariesGraphql);

  function handleResponsivePokedex(){
    if(!responsivePokedex){
      const pokedex = document.getElementsByClassName('pokemons__pokedex')[0];
      pokedex.style.bottom = '-10%';
      setResponsivePokedex(true)
    }else{
      const pokedex = document.getElementsByClassName('pokemons__pokedex')[0];
      pokedex.style.bottom = '-90%';
      setResponsivePokedex(false)
    }
  }

  function definePokemons(){
    switch(props.match.url){
      case '/pokemons':
        return filteredPokemons;
        break;
      case '/pokemons/strongest':
        return filteredStrongest
        break;
      case '/pokemons/weaknest':
        return filteredWeaknest
        break;
      case '/pokemons/legendaries':
        return filteredIsLegendaries
        break;
      default: 
        return filteredPokemons;
    }
  }

  function loadMorePokemons(){
    let currPage;
    switch (props.match.url) {
      case '/pokemons/strongest':
        currPage = currentPageStrongest+1;
        setCurrentPageStrongest(currPage);
        setLastsPokemons([]);
        NextStrongestPokemons({
          variables: {
            force: 2,
            page: currPage,
            pageNumber: 20
          }
        })
        break;

      case '/pokemons/weaknest':
        currPage = currentPageWeaknest+1;
        setCurrentPageWeaknest(currPage);
        setLastsPokemons([]);
        NextWeaknestPokemons({
          variables: {
            force: 0,
            page: currPage,
            pageNumber: 20
          }
        });
        break;

      case '/pokemons/legendaries':
        currPage = currentPageIslegendaries+1;
        setCurrentPageIsLegendaries(currPage);
        setLastsPokemons([]);
        NextLegendariesPokemons({
          variables: {
            type: "psychic",
            page: currPage,
            pageNumber: 20
          }
        });
        break;
    
      default:
        currPage = currentPage+1;
        setCurrentPage(currPage);
        setLastsPokemons([]);
        NextPokemons({
          variables: {
            page: currPage,
            pageNumber: 20
          }
        })
        break;
    }
  }

  if(dataNextPokemons && dataNextPokemons.getCards && lastsPokemons.length === 0){
    setLastsPokemons([...dataNextPokemons.getCards])
    setPokemons([...pokemons, ...dataNextPokemons.getCards])
    setFilteredPokemons([...pokemons, ...dataNextPokemons.getCards])
  }

  if(dataNextStrongestPokemons && dataNextStrongestPokemons.getByForce && lastsPokemons.length === 0){
    setLastsPokemons([...dataNextStrongestPokemons.getByForce])
    setPokemons([...pokemons, ...dataNextStrongestPokemons.getByForce])
    setStrongest([...strongest, ...dataNextStrongestPokemons.getByForce])
    setFilteredStrongest([...strongest, ...dataNextStrongestPokemons.getByForce])
  }

  if(dataNextWeaknestPokemons && dataNextWeaknestPokemons.getByForce && lastsPokemons.length === 0){
    setLastsPokemons([...dataNextWeaknestPokemons.getByForce])
    setPokemons([...pokemons, ...dataNextWeaknestPokemons.getByForce])
    setWeaknest([...weaknest, ...dataNextWeaknestPokemons.getByForce])
    setFilteredWeaknest([...weaknest, ...dataNextWeaknestPokemons.getByForce])
  }

  if(dataNextLegendariesPokemons && dataNextLegendariesPokemons.getLegendary && lastsPokemons.length === 0){
    setLastsPokemons([...dataNextLegendariesPokemons.getLegendary])
    setPokemons([...pokemons, ...dataNextLegendariesPokemons.getLegendary])
    setIsLegendaries([...isLegendaries, ...dataNextLegendariesPokemons.getLegendary])
    setFilteredIsLegendaries([...isLegendaries, ...dataNextLegendariesPokemons.getLegendary])
  }

  






  // ------------- Loading -------------
  // ------------- Loading -------------
  if(loading) return (
    <section className="pokemons">
      
      <div className="pokemons__pokemon">
        <Pokemon pokemon={currentPokemon}/>
      </div>
      <div className="pokemons__pokedex">
        <Pokedex 
          onClick={handleResponsivePokedex} 
          seeMore={responsivePokedex ? 'See Less' : 'See More'}
          pokemons={definePokemons()}
          // loadMore={loadMorePokemons}
        />
      </div>
    </section>
  )






  // ------------- Error -------------
  // ------------- Error -------------
  if(error) return 'error'






  // ------------- Fetched Data -------------
  // ------------- Fetched Data -------------
  if(data){
    if(data.getCards && pokemons.length === 0){
      setPokemons([...pokemons, ...data.getCards]);
      setFilteredPokemons([...data.getCards]);
    }
  }

  return (
    <section className="pokemons">
      
      <div className="pokemons__pokemon">
        <Pokemon pokemon={currentPokemon}/>
      </div>
      <div className="pokemons__pokedex">
        <Pokedex 
          onClick={handleResponsivePokedex} 
          seeMore={responsivePokedex ? 'See Less' : 'See More'}
          pokemons={definePokemons()}
          loadMore={loadMorePokemons}
          loading={loadingNextPokemons}
        />
      </div>
    </section>
  )
}

export default Pokemons;
