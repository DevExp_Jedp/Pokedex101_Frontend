import React, { Component } from "react";

import Header from "../components/Header/Header";
import SurpriseOption from "../components/CreateOptions/SurpriseOption";
import ComingSoonOption from "../components/CreateOptions/ComingSoonOption";
import { creatingPokemon } from "../services/Manifest";

import { PokemonsContext } from "../providers/PokemonProviders";

class Create extends Component {
  constructor(props) {
    super(props);
    this.goToCreatePokemon = this.goToCreatePokemon.bind(this);
    this.state = {
      setCreatePokemon: null,
    };
  }

  componentDidMount() {
    const [createPokemon, setCreatePokemon] = this.context.createPokemon;
    this.setState({
      setCreatePokemon,
    });
  }

  goToCreatePokemon() {
    this.props.history.push("/create/surprise");
    this.state.setCreatePokemon({ ...creatingPokemon });
  }

  render() {
    return (
      <>
        <Header />
        <section className="create-section">
          <div className="create-section__surprise">
            <SurpriseOption goTo={this.goToCreatePokemon} />
          </div>
          <div className="create-section__personalize">
            <ComingSoonOption />
          </div>
        </section>
      </>
    );
  }
}
Create.contextType = PokemonsContext;

export default Create;
