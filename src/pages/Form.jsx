import React from 'react'
// Components
import FormPokemon from '../components/FormPokemon/FormPokemon';
import PreviewPokemon from '../components/PreviewPokemon/PreviewPokemon';

class Form extends React.Component {

  constructor(props){
    super(props);
    this.goToCreatedAction = this.goToCreatedAction.bind(this);
  }

  goToCreatedAction(){
    this.props.history.push('/created');
  }

  render() {
    return (
      <section className="create">
        <div className="create__pokemon">
          <PreviewPokemon />
        </div>
        <div className="create__form">
          <FormPokemon action={this.goToCreatedAction} />
        </div>
      </section>
    )
  }
}

export default Form;
