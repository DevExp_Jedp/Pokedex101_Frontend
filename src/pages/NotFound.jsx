import React from 'react'

import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import Button from '../components/Button/Button';

import EmptyPokeball from '../assets/images/Pokeball-normal-open.png';

class NotFound extends React.Component {
  render() {
    return (
      <>
        <Header />
        <div className="NotFound">
          <figure aria-label="Error, page not found">
            <img src={EmptyPokeball} alt="Error, page not found" aria-label="Error, page not found" />
          </figure>
          <strong className="h2">Error 404 :(</strong>
          <h2 className="strong">We Sorry, This page has scaped</h2>
          
          <Button title="Go Back" />
        </div>
        <Footer />
      </>
    )
  }
}

export default NotFound;
