function saveImage(img) {
  const imgPreview = document.getElementById('image-preview-pokemon');
  const reader = new FileReader();

  reader.addEventListener('load', async function () {
    if (this.result && localStorage) {
      await localStorage.setItem('img_url', this.result);
      await imgPreview.setAttribute('src', localStorage.getItem('img_url'));
    }
  })

  reader.readAsDataURL(img);
}

export default saveImage;