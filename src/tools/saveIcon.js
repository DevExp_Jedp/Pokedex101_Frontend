export default async function SaveIcon(img) {
  const iconPreview = document.getElementById('icon-preview-pokemon');
  const reader = new FileReader();

  reader.addEventListener('load', async function () {
    if (this.result && localStorage) {
      await localStorage.setItem('icon_img', this.result);
      await iconPreview.setAttribute('src', localStorage.getItem('icon_img'));
    } else {
      console.log('Error, the images can\'t be uploaded in the localStorage');
    }
  })

  await reader.readAsDataURL(img);
}