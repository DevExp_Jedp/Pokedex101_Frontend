import React from "react";
import { NavLink } from "react-router-dom";

const SmallHeader = () => {
  return (
    <header className="small-header">
      <ul>
        <li>
          <NavLink exact={true} activeClassName="selected" to="/">
            Home
          </NavLink>
        </li>
        <li>
          <NavLink exact={true} activeClassName="selected" to="/pokemons">
            Pokedex
          </NavLink>
        </li>
        <li>
          <NavLink exact={true} activeClassName="selected" to="/categories">
            Categories
          </NavLink>
        </li>
        <li>
          <NavLink exact={true} activeClassName="selected" to="/create">
            Create
          </NavLink>
        </li>
      </ul>
    </header>
  );
};

export default SmallHeader;
