import React, { useContext } from "react";

// Images
import miniPikachu from "../../assets/images/MiniPikachu.png";
import TagType from "../TagType/TagType";

import { PokemonsContext } from "../../providers/PokemonProviders";

const CardPokemon = ({
  name,
  pokedexNumber,
  type,
  imgUrl,
  secondaryAction,
}) => {
  const context = useContext(PokemonsContext);
  const [pokemon, setPokemon] = context.pokemon;
  const [pokemons, setPokemons] = context.pokemons;

  function handleCurrentPokemon() {
    // console.log(pokemons.filter(pokemon => pokemon.pokemon_number === pokedexNumber))
    const currPokemon = pokemons.filter(
      (pokemon) => pokemon.pokemon_number === pokedexNumber
    );
    setPokemon(currPokemon.pop());
  }

  return (
    <button
      className="card-pokemon"
      aria-label="Pokemon"
      onClick={secondaryAction || handleCurrentPokemon}
    >
      <span className="small card-pokemon__number">
        {pokedexNumber || "01"}.
      </span>
      <figure className="card-pokemon__image">
        <img src={imgUrl} alt={name} />
      </figure>
      <h4 className="card-pokemon__name">{name || "Pikachu"}</h4>
      <div className="card-pokemon__type">
        <TagType type={type || "fairy"} />
      </div>
    </button>
  );
};

export default CardPokemon;
