import React, { useContext, useEffect } from "react";
import { PokemonsContext } from "../../providers/PokemonProviders";

// components
import SmallHeader from "../SmallHeader/SmallHeader";
import TagType from "../TagType/TagType";

// Image
import Pokeball from "../../assets/images/Pokeball-normal.png";

const CreatePokemon = () => {
  const { createdPokemon, pokemons } = useContext(PokemonsContext);
  const [newCreatedPokemon, setNewCreatedPokemon] = createdPokemon;
  const [currentPokemons, setCurrentPokemons] = pokemons;

  // useEffect(() => {
  //   setCurrentPokemons({
  //     ...currentPokemons,
  //     newCreatedPokemon,
  //   });
  // }, []);

  return (
    <article className={`create-pokemon bg-electric`}>
      <SmallHeader />
      <div className="create-pokemon__name">
        <figure>
          <img src={Pokeball} alt="pokeball" />
        </figure>
        <h1 className="h4">
          {newCreatedPokemon.english_name || "Name Of Pokemon"}
        </h1>
      </div>

      <div className="create-pokemon__general-info">
        <p>
          Exp: <small>{newCreatedPokemon.experience || 1011100}</small>
        </p>
        <p>
          Type:{" "}
          {newCreatedPokemon.type.length === 0 ? (
            <small>There is no Type</small>
          ) : (
            newCreatedPokemon.type.map((type, i) => (
              <TagType key={i} type={type} />
            ))
          )}{" "}
        </p>
      </div>

      <div className="create-pokemon__main">
        <figure className="create-pokemon__images">
          <img
            src={newCreatedPokemon.image_url}
            alt={newCreatedPokemon.english_name}
            className="images-pokemon"
          />
          <img
            src={newCreatedPokemon.icon_url}
            alt={newCreatedPokemon.english_name}
            className="images-mini-pokemon"
          />
        </figure>

        <ul className="create-pokemon__main-stats">
          <li>
            <p>
              Attack: <small>{newCreatedPokemon.attack || "0"} pts</small>
            </p>
          </li>
          <li>
            <p>
              Defense: <small>{newCreatedPokemon.defense || "0"} pts</small>
            </p>
          </li>
          <li>
            <p>
              Speed: <small>{newCreatedPokemon.speed || "0"} pts</small>
            </p>
          </li>
        </ul>
      </div>

      <p>Generation {newCreatedPokemon.generation}</p>

      <div className="create-pokemon__specific-stats">
        <ul className="create-pokemon__specifics-stats-abilities">
          <li className="strong">Abilities:</li>
          {newCreatedPokemon.abilities.length === 0 ? (
            <small>There is no Abilities.</small>
          ) : (
            newCreatedPokemon.abilities.map((ability, index) => (
              <li key={index}>
                <p>{ability || "poison"}</p>
              </li>
            ))
          )}
        </ul>
        <ul className="create-pokemon__specifics-stats-information">
          <li className="strong">Information:</li>
          <li>
            <p>weight: {newCreatedPokemon.weight_kg || "0"}</p>
          </li>
          <li>
            <p>height: {newCreatedPokemon.height_m || "0"}</p>
          </li>
        </ul>
        <ul className="create-pokemon__specifics-stats-genre">
          <li className="strong">Genre:</li>
          <li>
            <p>Male: {newCreatedPokemon.percentage_male || "0"}%</p>
          </li>
          <li>
            <p>Female: {newCreatedPokemon.percentage_female || "0"}%</p>
          </li>
        </ul>
      </div>
    </article>
  );
};

export default CreatePokemon;
