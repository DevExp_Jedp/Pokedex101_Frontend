import React, { useState } from "react";
import { NavLink } from "react-router-dom";

const Header = () => {
  const [menuOpen, setMenuOpen] = useState(false);

  function handleMenu() {
    const menu = document.getElementsByClassName("header__menu")[0];

    if (!menuOpen) {
      menu.style.left = "0%";
      menu.style.opacity = "1";
      setMenuOpen(true);
    } else {
      menu.style.left = "-100%";
      menu.style.opacity = "0";
      setMenuOpen(false);
    }
  }

  return (
    <header className="header">
      <a href="#content" className="header__skip">
        Skip To Content
      </a>
      <nav className="header__nav">
        <ul>
          <li>
            <NavLink exact to="/" activeClassName="selected">
              Home
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/pokemons" activeClassName="selected">
              Pokedex
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/categories" activeClassName="selected">
              Categories
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/create" activeClassName="selected">
              Create
            </NavLink>
          </li>
        </ul>
      </nav>

      <div className="strong header__menu--btn" onClick={handleMenu}>
        menu
      </div>

      <div className="header__menu" aria-live="off" tabIndex="-1">
        <ul>
          <li>
            <NavLink tabIndex="-1" exact to="/" activeClassName="selected">
              Home
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              to="/pokemons"
              activeClassName="selected"
              tabIndex="-1"
            >
              Pokedex
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              to="/categories"
              activeClassName="selected"
              tabIndex="-1"
            >
              Categories
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              to="/create"
              activeClassName="selected"
              tabIndex="-1"
            >
              Create
            </NavLink>
          </li>
        </ul>
        <small onClick={handleMenu} tabIndex="-1">
          Close
        </small>
      </div>

      <h1 className="small" tabIndex="-1">
        Pokedex 101
      </h1>
    </header>
  );
};

export default Header;
