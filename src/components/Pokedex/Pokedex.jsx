import React, { useContext } from "react";

// Components
import FormPokedex from "../FormPokedex/FormPokedex";
import CardPokemon from "../CardPokemon/CardPokemon";
import ArrowLeft from "../Arrows/ArrowLeft";
import Loading from "../Loading/Loading";
import Button from "../Button/Button";
import NotFound from "../NotFound/NotFound";

import { PokemonsContext } from "../../providers/PokemonProviders";

function Pokedex({ onClick, seeMore, pokemons, loadMore, loading }) {
  const context = useContext(PokemonsContext);
  const [pokemonsContext, setPokemons] = context.pokemons;
  const [filteredPokemons, setFilteredPokemons] = context.filteredPokemons;
  const [filteredStrongest, setFilteredStrongest] = context.filteredStrongest;
  const [filteredWeaknest, setFilteredWeaknest] = context.filteredWeaknest;
  const [
    filteredIsLegendaries,
    setFilteredIsLegendaries,
  ] = context.filteredIsLegendaries;

  return (
    <article className="pokedex">
      <div className="pokedex__title">
        <h2 className="h3">Pokedex 101</h2>
        <span
          onClick={onClick}
          className={seeMore === "See More" ? "arrow-up" : "arrow-down"}
        >
          <ArrowLeft />
        </span>
      </div>
      <div className="pokedex__form">
        <FormPokedex />
      </div>

      <h4>Pokemons</h4>
      <div className="pokedex__pokemons">
        {pokemonsContext.length === 0 ? (
          <Loading />
        ) : (
          pokemons.map((pokemon, index) => (
            <CardPokemon
              key={index}
              name={pokemon.english_name}
              pokedexNumber={pokemon.pokemon_number}
              type={pokemon.type[0]}
              imgUrl={pokemon.icon_url}
            />
          ))
        )}
        {pokemons.length != 0 && (
          <Button title="Load More" onClick={loadMore} />
        )}
        {filteredPokemons.length === 0 && <NotFound />}
      </div>
    </article>
  );
}

export default Pokedex;
