import React, {useState} from 'react'
import { Link } from 'react-router-dom'

import Button from '../Button/Button';
import SmallCardPokemon from '../SmallCardPokemon/SmallCardPokemon';

import MiniPikachu from '../../assets/images/MiniPikachu.png';

const CategoryCard = ({type, title, description, onClick, pokemon, miniPokemon, pokemons, second_description}) => {

  const [cardIsFlipped, setCardIsFlipped] = useState(false);

  const handleCard3D = (ev) => {
    const card3D = ev.currentTarget;
    // console.log(ev.currentTarget)
    if(!cardIsFlipped) {
      card3D.classList.add('is-flipped')
      setCardIsFlipped(true)
    }else {
      card3D.classList.remove('is-flipped')
      setCardIsFlipped(false)
    }

  }

  return (
    <article className="category-card" tabIndex="0">

    {/* Category Card card */}
      <div className="category-card--card" onClick={handleCard3D}>

      {/* Category Face Front */}
        <div className={`category-card-${type} category-card--face category-card--front`} >
          <h2 className="h4 category-card__title">{title || 'Title Card'}</h2>
          <figure className="category-card__images">
            <img className="category-card__images-pokemon" src={pokemon} alt={title || 'Img Pokemon'} aria-label={title || 'Img Pokemon'} />
            <img className="category-card__images-mini-pokemon" src={miniPokemon} alt={title || 'Img Pokemon'} aria-label={title || 'Img Pokemon'} />
          </figure>
          <p className="category-card__text">
            {description || 'Description of the card'}
          </p>
          <Button title="See More" />
        </div>
        {/* Category Card Face Back */}
        <div className={`category-card-${type} category-card--face category-card--back`}>
          <h2 className="h4 category-card__title">{title || 'Title Card'}</h2>
          <ul className="category-card__list">
            {
              !pokemons ? ('loading') : (pokemons.slice(0, 3).map((pokemon, i) => (
                <li key={i}>
                  <SmallCardPokemon img={pokemon.icon_url} number={i+1} name={pokemon.english_name} />
                </li>
              )))
            }

          </ul>
          <p className="category-card__description">
            {second_description}
          </p>
          <Button title="See All" onClick={onClick} />

        </div>
        
      </div>
    </article>
  )
}

export default CategoryCard
