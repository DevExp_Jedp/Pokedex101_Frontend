import React, { useContext, useState } from "react";
// Tools
import { ManifestTypes, creatingPokemon } from "../../services/Manifest";
import saveImage from "../../tools/saveImage";
import SaveIcon from "../../tools/saveIcon";

// Apollo
import { useMutation } from "@apollo/client";
import CreatePokemonGraphql from "../../services/CreatePokemon.graphql";

// Components
import ButtonAddFrom from "../ButtonAddForm/ButtonAddForm";
import TagType from "../TagType/TagType";
import Loading from "../Loading/Loading";

// Context
import { PokemonsContext } from "../../providers/PokemonProviders";

const FormPokemon = ({ action }) => {
  // Context
  const {
    createPokemon,
    createdPokemon,
    pokemons,
    filteredPokemons,
  } = useContext(PokemonsContext);
  const [pokemon, setPokemon] = createPokemon;
  const [newCreatedPokemon, setNewCreatedPokemon] = createdPokemon;
  const [currentPokemons, setCurrentPokemons] = pokemons;
  const [
    currentFilteredPokemons,
    setCurrentFilteredPokemons,
  ] = filteredPokemons;

  // States
  const [addAbility, setAbility] = useState(false);
  const [addType, setType] = useState(false);
  const [maleGenre, setMaleGenre] = useState(0);
  const [femaleGenre, setFemaleGenre] = useState(0);

  // Apollo
  const [submitForm, { data, loading }] = useMutation(CreatePokemonGraphql);

  function handleAddAbility(ev) {
    if (addAbility) {
      setAbility(false);
    } else {
      setAbility(true);
    }
  }
  function handleNewAbility() {
    const newAbility = document.getElementById("newAbility");
    setPokemon({
      ...pokemon,
      abilities: [...pokemon.abilities, newAbility.value],
    });
    setAbility(false);
  }

  function handleAddType(ev) {
    if (addType) {
      setType(false);
    } else {
      setType(true);
    }
  }
  function handleNewType() {
    const newType = document.getElementById("newType");
    setPokemon({
      ...pokemon,
      type: [...pokemon.type, newType.value],
    });
    setType(false);
  }

  function handleNameOfPokemon(ev) {
    setPokemon({
      ...pokemon,
      english_name: ev.currentTarget.value,
    });
  }

  function handleHeightPokemon(ev) {
    setPokemon({
      ...pokemon,
      height: ev.currentTarget.value.toString() + "m",
    });
  }
  function handleWeightPokemon(ev) {
    setPokemon({
      ...pokemon,
      weight: ev.currentTarget.value.toString() + "kg",
    });
  }
  function handleGenerationPokemon(ev) {
    setPokemon({
      ...pokemon,
      generation: ev.currentTarget.value.toString(),
    });
  }

  async function handleIconImage(ev) {
    await SaveIcon(ev.currentTarget.files[0]);
  }
  async function handleMainImage(ev) {
    await saveImage(ev.currentTarget.files[0]);
  }

  function handleMaleGenre(ev) {
    setMaleGenre(ev.currentTarget.value.toString() + "%");
  }
  function handleFemaleGenre(ev) {
    setFemaleGenre(ev.currentTarget.value.toString() + "%");
  }

  function handleSubmitForm(ev) {
    ev.preventDefault();

    const iconPokemon = document.getElementById("icon-pokemon-form");
    const imgPokemon = document.getElementById("image-pokemon-form");

    console.log(iconPokemon.files[0], "Icon");
    console.log(iconPokemon.files, "Icon");

    try {
      submitForm({
        variables: {
          name: pokemon.english_name,
          type: pokemon.type,
          icon: iconPokemon.files[0],
          image: imgPokemon.files[0],
          weight: pokemon.weight,
          height: pokemon.height,
          abilities: pokemon.abilities,
          generation: pokemon.generation,
          percentage_male: maleGenre,
          percentage_female: femaleGenre,
        },
      });
    } catch (err) {
      console.log(err, "error - The data can't be uploaded");
    }
  }

  // If the response exists this happen =>
  async function actionComplete() {
    localStorage.removeItem("img_url");
    localStorage.removeItem("icon_img");
    setPokemon({ ...creatingPokemon });
    await data.createPokemon;
    setNewCreatedPokemon(data.createPokemon);
    setCurrentPokemons([...currentPokemons, data.createPokemon]);
    setCurrentPokemons([...currentFilteredPokemons, data.createPokemon]);
    action();
  }

  if (data && !loading && data.createPokemon) actionComplete();

  return (
    <form
      className="form-pokemon"
      encType="multipart/form-data"
      onSubmit={handleSubmitForm}
      id="create-pokemon-form"
      method="POST"
    >
      <h1 className="h4">Create Your Pokemon</h1>

      <div className="form-pokedex__general-info">
        <label htmlFor="name" className="general-info--name">
          <small>Name Of Pokemon</small>
          <input
            type="text"
            name="name"
            id="name"
            placeholder="Write a name for the pokemon"
            aria-label="Write a name for the pokemon"
            onChange={handleNameOfPokemon}
            required
          />
        </label>
      </div>

      {/* Main Stats */}
      {/* Main Stats */}
      <div className="form-pokedex__stats">
        <label htmlFor="experience" aria-label="Experience of the pokemon">
          <small>Height:</small>
          <input
            type="number"
            placeholder="Mts"
            onChange={handleHeightPokemon}
            required
            max="20"
            min="0"
          />
        </label>

        <label htmlFor="attack" aria-label="attack of the pokemon">
          <small>Weight:</small>
          <input
            type="number"
            placeholder="Kg"
            onChange={handleWeightPokemon}
            max="999"
            min="0"
            required
          />
        </label>

        <label htmlFor="attack" aria-label="attack of the pokemon">
          <small>Generation:</small>
          <input
            type="number"
            placeholder="Generation"
            onChange={handleGenerationPokemon}
            required
          />
        </label>
      </div>

      <div className="form-pokemon__specific-info">
        <div>
          <h3 className="strong">Abilites</h3>
          <label
            htmlFor="abilities"
            aria-label="Abilities of the pokemon"
            className="form-pokemon__specific-info--abilities"
          >
            <div className="general-info--abilities">
              <ul>
                {pokemon.abilities.length === 0 ? (
                  <small>There is no abilities</small>
                ) : (
                  pokemon.abilities.map((ability, index) => (
                    <li key={index} className="small">
                      {ability}
                    </li>
                  ))
                )}
              </ul>
              {addAbility ? (
                <input type="text" placeholder="Add Ability" id="newAbility" />
              ) : (
                <ButtonAddFrom
                  title={"+ Add Ability"}
                  action={handleAddAbility}
                />
              )}
              {addAbility && (
                <ButtonAddFrom title={"Save"} action={handleNewAbility} />
              )}
            </div>
          </label>
        </div>

        {/* New Type */}
        <div>
          <h3 className="strong">Type</h3>
          <label
            htmlFor="abilities"
            aria-label="Abilities of the pokemon"
            className="form-pokemon__specific-info--types"
          >
            <div className="general-info--types">
              <ul>
                {pokemon.type.length === 0 ? (
                  <small>There is no type</small>
                ) : (
                  pokemon.type.map((type, index) => (
                    <li key={index} className="small">
                      <TagType type={type} />
                    </li>
                  ))
                )}
              </ul>
              {addType ? (
                <select id="newType">
                  {ManifestTypes.map((type, i) => (
                    <option value={type} key={i}>
                      {type}
                    </option>
                  ))}
                </select>
              ) : (
                <ButtonAddFrom title={"+ Add Type"} action={handleAddType} />
              )}
              {addType && (
                <ButtonAddFrom title={"Save"} action={handleNewType} />
              )}
            </div>
          </label>
        </div>

        {/* Genre */}
        {/* Genre */}
        <div>
          <h3 className="strong">Genre</h3>
          <div className="general-info--types">
            <label
              htmlFor="abilities"
              aria-label="Abilities of the pokemon"
              className="form-pokemon__specific-info--types"
            >
              <div
                className="form-pokedex__stats--ranges"
                aria-label="minimum experience"
              >
                <small>M:</small>
                <input
                  type="range"
                  min="0"
                  max="100"
                  placeholder="Male"
                  onChange={handleMaleGenre}
                />
              </div>
              <div className="form-pokedex__stats--ranges">
                <small>F:</small>
                <input
                  type="range"
                  min="0"
                  max="100"
                  placeholder="Female"
                  onChange={handleFemaleGenre}
                />
              </div>
            </label>
          </div>
        </div>
      </div>

      <div className="form-pokemon__images">
        <h3 className="strong">Images</h3>
        <p className="small">Select the images of the new pokemon!</p>
        <div className="form-pokemon__images--imgs">
          <label htmlFor="image">
            <small>Icon:</small>
            <input
              type="file"
              formEncType="multipart/form-data"
              accept="image/*"
              placeholder="Upload the image of the pokemon"
              onChange={handleIconImage}
              required
              id="icon-pokemon-form"
            />
          </label>
          <label htmlFor="image">
            <small>Image:</small>
            <input
              type="file"
              accept="image/*"
              placeholder="Upload the image of the pokemon"
              onChange={handleMainImage}
              required
              id="image-pokemon-form"
            />
          </label>
        </div>
      </div>

      {/* <Button title="Create!" onClick={handleSubmitingForm} /> */}
      {loading ? (
        <div className="form-pokemon__loading">
          <Loading />
        </div>
      ) : (
        <button className="button-add" type="submit">
          Create!
        </button>
      )}
    </form>
  );
};

export default FormPokemon;
