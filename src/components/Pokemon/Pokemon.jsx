import React, { useState, useContext } from "react";

// Components
import TagType from "../TagType/TagType";
import Button from "../Button/Button";
import SmallHeader from "../SmallHeader/SmallHeader";
import SelectPokemon from "../SelectPokemon/SelectPokemon";

// Images
import Pokeball from "../../assets/images/Pokeball-normal.png";
import PikachuBg from "../../assets/images/Pikachu.png";
import MiniPikachu from "../../assets/images/MiniPikachu.png";

// Context
import { PokemonsContext } from "../../providers/PokemonProviders";

function Pokemon() {
  const context = useContext(PokemonsContext);
  const [pokemon, setPokemon] = context.pokemon;

  // States
  const [extraInformation, setExtraInformation] = useState(false);
  const [imgLoading, setImgLoading] = useState(true);

  const handleExtraInformation = () => {
    const specificStats = document.getElementsByClassName(
      "pokemon__specific-stats"
    )[0];
    const pokemonImage = document.getElementsByClassName("pokemon__images")[0];
    const imgPoke = document.getElementsByClassName("images-pokemon")[0];
    const miniPoke = document.getElementsByClassName("images-mini-pokemon")[0];

    if (!extraInformation) {
      specificStats.style.opacity = "1";
      specificStats.style.height = "auto";
      pokemonImage.style.minWidth = "90px";
      pokemonImage.style.minHeight = "90px";
      miniPoke.style.width = "36%";
      imgPoke.style.width = "90%";
      setExtraInformation(true);
    } else {
      specificStats.style.opacity = "0";
      specificStats.style.height = "0px";
      pokemonImage.style.minWidth = "240px";
      pokemonImage.style.minHeight = "240px";
      miniPoke.style.width = "71%";
      imgPoke.style.width = "120%";
      setExtraInformation(false);
    }
  };

  if (Object.entries(pokemon).length === 0)
    return (
      <section className={`bg-electric pokemon`}>
        <SmallHeader />
        <SelectPokemon />
      </section>
    );

  return (
    <section className={`bg-${pokemon.type[0]} pokemon`}>
      <SmallHeader />
      <div className="pokemon__name">
        <figure>
          <img src={Pokeball} alt="pokeball" />
        </figure>
        <h1 className="h4">{pokemon.english_name || "Pikachu"}</h1>
      </div>

      <div className="pokemon__general-info">
        <p>
          Exp: <small>{pokemon.experience || 1011100}</small>
        </p>
        <p>
          Type:{" "}
          {pokemon.type.map((type, i) => (
            <TagType key={i} type={type} />
          ))}{" "}
        </p>
      </div>

      <div className="pokemon__main">
        <figure className="pokemon__images">
          <img
            src={pokemon.image_url}
            loading="lazy"
            alt={pokemon.english_name}
            className="images-pokemon"
          />
          <img
            src={pokemon.icon_url}
            loading="lazy"
            alt={pokemon.english_name}
            className="images-mini-pokemon"
          />
        </figure>

        <ul className="pokemon__main-stats">
          <li>
            <p>
              Attack: <small>{pokemon.attack || "59"} pts</small>
            </p>
          </li>
          <li>
            <p>
              Defense: <small>{pokemon.defense || "59"} pts</small>
            </p>
          </li>
          <li>
            <p>
              Speed: <small>{pokemon.speed || "59"} pts</small>
            </p>
          </li>
        </ul>
      </div>

      <p>Generation {pokemon.generation}</p>

      <div className="pokemon__specific-stats">
        <ul className="pokemon__specifics-stats-abilities">
          <li className="strong">Abilities:</li>
          {pokemon.abilities.map((ability, index) => (
            <li key={index}>
              <p>{ability || "poison"}</p>
            </li>
          ))}
        </ul>
        <ul className="pokemon__specifics-stats-information">
          <li className="strong">Information:</li>
          <li>
            <p>weight: {pokemon.weight_kg || "84"}</p>
          </li>
          <li>
            <p>height: {pokemon.height_m || "100"}</p>
          </li>
        </ul>
        <ul className="pokemon__specifics-stats-genre">
          <li className="strong">Genre:</li>
          <li>
            <p>Male: {pokemon.percentage_male || "84"}%</p>
          </li>
          <li>
            <p>Female: {pokemon.percentage_female || "16"}%</p>
          </li>
        </ul>
      </div>

      <div className="pokemon__button">
        {extraInformation ? (
          <Button
            color="black"
            title="See Less"
            onClick={handleExtraInformation}
          />
        ) : (
          <Button
            color="black"
            title="See More"
            onClick={handleExtraInformation}
          />
        )}
      </div>
    </section>
  );
}

export default Pokemon;
