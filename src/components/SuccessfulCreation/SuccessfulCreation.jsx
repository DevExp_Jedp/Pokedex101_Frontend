import React, { useContext } from "react";
import { PokemonsContext } from "../../providers/PokemonProviders";

import CardPokemon from "../CardPokemon/CardPokemon";
import Button from "../Button/Button";

function SuccessfulCreation({ onClick }) {
  const { createdPokemon } = useContext(PokemonsContext);
  const [newCreatedPokemon, setNewCreatedPokemon] = createdPokemon;

  return (
    <article className="successful-creation">
      <h2>Pokemon Created!</h2>
      <CardPokemon
        name={newCreatedPokemon.english_name || "New Pokemon"}
        type={newCreatedPokemon.type[0] || ["electric"]}
        pokedexNumber={newCreatedPokemon.pokemon_number || "1"}
        imgUrl={newCreatedPokemon.icon_url}
        secondaryAction={() => false}
      />
      <strong>The pokemon name was created succesful! </strong>
      <Button title="Go Home!" onClick={onClick} />
    </article>
  );
}

export default SuccessfulCreation;
