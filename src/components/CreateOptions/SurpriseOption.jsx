import React from "react";

import Button from "../Button/Button";

import PokeballLegendaryImage from "../../assets/images/Pokeball-legendary.png";

function SurpriseOption({ goTo }) {
  return (
    <article tabIndex="0" className="bg-dragon surprise-option">
      <h1 className="h4" tabIndex="0">
        Surprise Pokemon
      </h1>
      <figure
        tabIndex="0"
        aria-label="Create a new random pokemon with the surprise pokeball"
      >
        <img
          src={PokeballLegendaryImage}
          alt="Surprise Pokeball - Create your pokemon"
        />
      </figure>
      <p tabIndex="0">
        Create the name and personalize the images of a new Pokemon!
      </p>
      <Button title="Create One!" onClick={goTo} />
    </article>
  );
}

export default SurpriseOption;
