import React from "react";

import PokeballNormalImage from "../../assets/images/Pokeball-normal.png";

function ComingSoonOption() {
  return (
    <article tabIndex="0" className="bg-normal coming-soon-option">
      <h2 tabIndex="0" className="h4">
        Personalize Your Own Pokemon
      </h2>
      <figure
        tabIndex="0"
        aria-label="Wait a little while. This feauture is coming soon!"
      >
        <img
          src={PokeballNormalImage}
          alt="Hold on! - This feature is coming soon"
        />
      </figure>
      <strong tabIndex="0" className="h5">
        Coming Soon!
      </strong>
    </article>
  );
}

export default ComingSoonOption;
