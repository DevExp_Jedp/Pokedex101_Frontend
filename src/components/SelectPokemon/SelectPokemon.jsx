import React from 'react'
import Pokeball from '../../assets/images/Pokeball-normal.png';

const SelectPokemon = () => {
  return (
    <div className="select-pokemon">

      <figure>
        <img src={Pokeball} alt="pokeball" />
      </figure>
      <h2>Select A Pokemon!</h2>
      
    </div>
  )
}

export default SelectPokemon;
