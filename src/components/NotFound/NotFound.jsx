import React from 'react'

// images
import PokeballOpen from '../../assets/images/Pokeball-normal-open.png';

const NotFound = () => {
  return (
    <div className="not-found-pokemon">
      <figure aria-label="Pokeball open, the pokemon has escaped">
        <img src={PokeballOpen} width="60px" alt="Pokeball open, the pokemon has escaped" />
      </figure>
      <h2 className="strong">
        We Sorry, the pokemon that you looking for has escaped :(
      </h2>
    </div>
  )
}

export default NotFound;
