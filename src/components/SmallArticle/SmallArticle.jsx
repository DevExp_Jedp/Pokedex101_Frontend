import React from 'react'

const SmallArticle = ({img, text, title, color}) => {
  return (
    <article className={`small-article--${color || 'white'} small-article`} tabIndex="0">
      <figure aria-label="Legendary Pokeball" className="small-article__image">
        <img src={img} alt={"Legendary Pokeball"} aria-label="legendary pokeball" />
      </figure>
      <strong className="strong small-article__title">{ title || 'Title Here' }</strong>
      <p className="text-paragraph small-article__text">
        {text || 'Text here of the small article please. Pass to props.'}
      </p>
    </article>
  )
}

export default SmallArticle;
