import React from 'react'

// Components
import ArrowRight from '../Arrows/ArrowRight'
import ArrowLeft from '../Arrows/ArrowLeft'

const Button = ({color, title, onClick}) => {
  return (
    <a className={`button button-${color}`} onClick={onClick} onKeyPress={onClick} tabIndex="0">
      <ArrowRight color={color || 'black'} />
      <strong>
        { title || 'title here'}
      </strong>
      <ArrowLeft color={color || 'black'}/>
    </a>
  )
}

export default Button
