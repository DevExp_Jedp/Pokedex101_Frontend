import React from 'react'

import Pokeball from '../../assets/images/Pokeball-normal.png';

const Loading = () => {
  return (
    <div className="loading">
      <figure aria-label="Loading">
        <img src={Pokeball} alt="Loading" />
      </figure>
      <h4>Loading...</h4>
    </div>
  )
}

export default Loading
