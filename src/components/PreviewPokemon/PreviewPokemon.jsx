import React, { useState, useContext } from "react";

// Components
import SmallHeader from "../SmallHeader/SmallHeader";
import ButtonAddForm from "../ButtonAddForm/ButtonAddForm";
import TagType from "../TagType/TagType";

// Image
import Pokeball from "../../assets/images/Pokeball-normal.png";

//Context
import { PokemonsContext } from "../../providers/PokemonProviders";

function PreviewPokemon() {
  const context = useContext(PokemonsContext);
  const [pokemon, setPokemon] = context.createPokemon;

  return (
    <article className={`create-pokemon bg-electric`}>
      <SmallHeader />
      <div className="create-pokemon__name">
        <figure>
          <img src={Pokeball} alt="pokeball" />
        </figure>
        <h1 className="h4" tabIndex="0">
          {pokemon.english_name || "Name Of Pokemon"}
        </h1>
      </div>
      <div className="create-pokemon__general-info">
        <p>
          Type:{" "}
          {pokemon.type.length === 0 ? (
            <small>There is no Type</small>
          ) : (
            pokemon.type.map((type, i) => <TagType key={i} type={type} />)
          )}{" "}
        </p>
      </div>

      <div className="create-pokemon__main">
        <figure className="create-pokemon__images" tabIndex="0">
          <img
            id="image-preview-pokemon"
            src={pokemon.image_url || localStorage.getItem("img_url")}
            alt={pokemon.english_name}
            className="images-pokemon"
            tabIndex="0"
          />
          <img
            id="icon-preview-pokemon"
            src={pokemon.icon_url || localStorage.getItem("icon_img")}
            alt={pokemon.english_name}
            className="images-mini-pokemon"
            tabIndex="0"
          />
        </figure>

        <ul className="create-pokemon__main-stats">
          <li className="strong">Information</li>
          <li tabIndex="0" aria-label="Height">
            Height: <small tabIndex="0">{pokemon.height || "0"}</small>
          </li>
          <li tabIndex="0" aria-label="Weight">
            Weight: <small tabIndex="0">{pokemon.weight || "0"}</small>
          </li>
        </ul>
      </div>

      <div className="create-pokemon__specific-stats">
        <ul className="create-pokemon__specifics-stats-abilities">
          <li className="strong" tabIndex="0" aria-label="Abilities">
            Abilities:
          </li>
          {pokemon.abilities.length === 0 ? (
            <small tabIndex="0">There is no Abilities.</small>
          ) : (
            pokemon.abilities.map((ability, index) => (
              <li key={index} tabIndex="0">
                <p>{ability || false}</p>
              </li>
            ))
          )}
        </ul>
      </div>
    </article>
  );
}

export default PreviewPokemon;
