import React from 'react'

function ButtonAddForm({title, action}) {
  return (
    <button title={title || 'Title here'} onClick={action} className="button-add" tabIndex="0">
      {title || 'Title here!'}
    </button>
  )
}

export default ButtonAddForm
