import React from 'react'

const TagType = ({type}) => {
  return (
    <span className={`type-${type}`}>
      {type}
    </span>
  )
}

export default TagType
