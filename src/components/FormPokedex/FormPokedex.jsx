import React, { useState, useContext } from "react";
import { PokemonsContext } from "../../providers/PokemonProviders";

// Apollo
import { useLazyQuery } from "@apollo/client";

// Graphql
import SearchPokemons from "../../services/SearchPokemons.graphql";

// Manifest
import { ManifestTypes } from "../../services/Manifest";

const FormPokedex = () => {
  // ------------ context -----------
  // ------------ context -----------
  const context = useContext(PokemonsContext);

  const [pokemons, setPokemons] = context.pokemons;
  const [filteredPokemons, setFilteredPokemons] = context.filteredPokemons;
  const [strongest, setStrongest] = context.strongest;
  const [filteredStrongest, setFilteredStrongest] = context.filteredStrongest;
  const [weaknest, setWeaknest] = context.weaknest;
  const [filteredWeaknest, setFilteredWeaknest] = context.filteredWeaknest;
  const [isLegendaries, setIsLegendaries] = context.isLegendaries;
  const [
    filteredIsLegendaries,
    setFilteredIsLegendaries,
  ] = context.filteredIsLegendaries;

  // ------------ state -----------
  // ------------ state -----------
  const [name, setName] = useState(/[A-Za-z]{1,}/);
  const [type, setType] = useState(/[A-Za-z]{1,}/);
  const [minAttack, setMinAttack] = useState(5);
  const [maxAttack, setMaxAttack] = useState(185);
  const [minExperience, setMinExperience] = useState(600000);
  const [maxExperience, setMaxExperience] = useState(1640000);

  // Apollo
  // const [searchPokemons, {loading, data}] = useLazyQuery(SearchPokemons);

  function handleSearchPokemons(
    namePokemon = /[A-Za-z]{1,}/,
    typePokemon = /[A-Za-z]{1,}/,
    minAttack = 5,
    maxAttack = 185,
    minExperience = 600000,
    maxExperience = 1640000
  ) {
    let attack = [minAttack, maxAttack];
    let filterPokemons;
    switch (location.pathname) {
      case "/pokemons/strongest":
        filterPokemons = strongest.filter(
          (pokemon) =>
            pokemon.english_name.toLowerCase().match(namePokemon) &&
            pokemon.type[0].toLowerCase().match(typePokemon) &&
            pokemon.attack >= minAttack &&
            pokemon.attack <= maxAttack &&
            pokemon.experience >= minExperience &&
            pokemon.experience <= maxExperience
        );
        setFilteredStrongest([...filterPokemons]);
        break;
      case "/pokemons/weaknest":
        filterPokemons = weaknest.filter(
          (pokemon) =>
            pokemon.english_name.toLowerCase().match(namePokemon) &&
            pokemon.type[0].toLowerCase().match(typePokemon) &&
            pokemon.attack >= minAttack &&
            pokemon.attack <= maxAttack &&
            pokemon.experience >= minExperience &&
            pokemon.experience <= maxExperience
        );
        setFilteredWeaknest([...filterPokemons]);
        break;
      case "/pokemons/legendaries":
        filterPokemons = isLegendaries.filter(
          (pokemon) =>
            pokemon.english_name.toLowerCase().match(namePokemon) &&
            pokemon.type[0].toLowerCase().match(typePokemon) &&
            pokemon.attack >= minAttack &&
            pokemon.attack <= maxAttack &&
            pokemon.experience >= minExperience &&
            pokemon.experience <= maxExperience
        );
        setFilteredIsLegendaries([...filterPokemons]);
        break;

      default:
        filterPokemons = pokemons.filter(
          (pokemon) =>
            pokemon.english_name.toLowerCase().match(namePokemon) &&
            pokemon.type[0].toLowerCase().match(typePokemon) &&
            pokemon.attack >= minAttack &&
            pokemon.attack <= maxAttack &&
            pokemon.experience >= minExperience &&
            pokemon.experience <= maxExperience
        );
        setFilteredPokemons([...filterPokemons]);
        break;
    }
  }

  function handleSubmitForm(ev) {
    ev.preventDefault();
    setType(ev.currentTarget.value);
    handleSearchPokemons(name, ev.currentTarget.value);
  }

  function handleNamePokemon(ev) {
    console.log(ev.currentTarget.value);
    setName(ev.currentTarget.value);
    handleSearchPokemons(ev.currentTarget.value.toLowerCase(), type);
  }

  function handleTypePokemon(ev) {
    ev.preventDefault();
    setType(ev.currentTarget.value);
    handleSearchPokemons(name, ev.currentTarget.value);
  }

  function handleMaxAttackPokemon(ev) {
    setMaxAttack(ev.currentTarget.value);
    handleSearchPokemons(name, type, minAttack, ev.currentTarget.value);
  }
  function handleMinAttackPokemon(ev) {
    setMinAttack(ev.currentTarget.value);
    handleSearchPokemons(name, type, ev.currentTarget.value, maxAttack);
  }

  function handleMinExperience({ currentTarget }) {
    console.log(currentTarget.value);
    setMinExperience(currentTarget.value);
    handleSearchPokemons(
      name,
      type,
      minAttack,
      maxAttack,
      currentTarget.value,
      maxExperience
    );
  }
  function handleMaxExperience({ currentTarget }) {
    setMaxExperience(currentTarget.value);
    handleSearchPokemons(
      name,
      type,
      minAttack,
      maxAttack,
      minExperience,
      currentTarget.value
    );
  }

  return (
    <form className="form-pokedex" onSubmit={handleSubmitForm}>
      <div className="form-pokedex__general-info">
        <label htmlFor="name">
          <small className="text-normal">Name Of Pokemon</small>
          <input
            type="text"
            name="name"
            id="name"
            placeholder="Search your favorite pokemon"
            aria-label="Search your favorite pokemon"
            onChange={handleNamePokemon}
          />
        </label>
        <label htmlFor="type" aria-label="Type of the pokemon">
          <small className="text-normal">Type</small>
          <select
            name="type"
            id="type"
            aria-label="Type of the pokemon"
            onChange={handleTypePokemon}
          >
            <option value="">All</option>
            {ManifestTypes.map((type, index) => (
              <option key={index} value={type}>
                {type}
              </option>
            ))}
          </select>
        </label>
      </div>

      <div className="form-pokedex__stats">
        <label htmlFor="experience" aria-label="Experience of the pokemon">
          <small className="text-normal">Experience</small>
          <div
            className="form-pokedex__stats--ranges"
            aria-label="minimum experience"
          >
            <small>Min:</small>
            <input
              type="range"
              min="600000"
              max={maxExperience}
              placeholder="Experiecne"
              value={minExperience}
              onChange={handleMinExperience}
            />
          </div>
          <div className="form-pokedex__stats--ranges">
            <small>Max:</small>
            <input
              type="range"
              min="600000"
              max="1640000"
              placeholder="Experience"
              value={maxExperience}
              onChange={handleMaxExperience}
            />
          </div>
        </label>

        <label htmlFor="attack" aria-label="attack of the pokemon">
          <small className="text-normal">Attack</small>
          <div className="form-pokedex__stats--ranges">
            <small>Min:</small>
            <input
              type="range"
              min="5"
              max={maxAttack}
              placeholder="Attack"
              value={minAttack}
              onChange={handleMinAttackPokemon}
            />
          </div>
          <div className="form-pokedex__stats--ranges">
            <small>Max:</small>
            <input
              type="range"
              min="5"
              max="185"
              placeholder="Attack"
              value={maxAttack}
              onChange={handleMaxAttackPokemon}
            />
          </div>
        </label>
      </div>
    </form>
  );
};

export default FormPokedex;
