import React from 'react'

const SmallCardPokemon = ({number, img, name}) => {
  return (
    <div className="small-card-pokemon" tabIndex="0">
      <span className="strong"> {number || '01'}. </span>
      <strong className="strong">
        {name || 'Pikachu'}
      </strong>
      <figure>
        <img src={img} alt={name || 'pikachu'} aria-label={name || 'pikachu'} />
      </figure>
    </div>
  )
}

export default SmallCardPokemon
