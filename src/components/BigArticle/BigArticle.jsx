import React from 'react'

const BigArticle = ({title, text, color, position}) => {
  return (
    <article className={`big-article--${color || 'black'} big-article--${position || 'center'} big-article`} tabIndex="0">
      <h2 className="big-article__title">
        WHY POKEDEX 101?
      </h2>
      <p className="text-paragraph big-article__text">
        {
          text || 'Here is the text of this article, if you see this pass per props the text that you need.'
        }
      </p>
    </article>
  )
}

export default BigArticle
