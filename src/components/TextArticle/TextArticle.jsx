import React from 'react'

// Components
import Button from '../Button/Button';

const TextArticle = ({title, description, position, onClick}) => {
  return (
    <article className={`text-article text-article--${position || 'right'}`}>
      <h2 className="h3">
        {title || "Find Your Pokemon"}
      </h2>
      <p className="text-paragraph">
        {description || "Hello there"}
      </p>
      <Button title="Start Looking" color="black" onClick={onClick} />
    </article>
  )
}

export default TextArticle
