import React from 'react'

const Footer = () => {
  return (
    <footer className="footer">
      <small tabIndex="0">
        © Copyright - 2020, Pokedex 101
      </small>
    </footer>
  )
}

export default Footer
