import React from 'react';

export const PokeballNormal = () => (
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="126" height="126" viewBox="0 0 126 126">
  <image width="126" height="126" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAA3UlEQVQokWP8v3wFAzKwnz7tPzL/YGYWIzKfBcaAKZwdfYhBPh4i9nAhSJzhP4pGkA12dnb/fzey/v/xgwGMJ06cCMYw/rcEhv8gNSC1TDAbmBX0GXABVkVWuAwjSOde5+NwDb/Cz6BoY1tpwvD3wUWG3/d/M3jcs4PYgK7Yzc0OjLEZANbgMJcLRfGuXbsYgoOD4Zr+lkNMh4cSSIGbmxvclJkzZ8LZME0gA9auXcsAjgdQkII0IRRBNIMUwQBIMSho4aGEbCrMSdgAPKZhEYeuEGQycsSRljQYGBgAFdZ3TYlRx7UAAAAASUVORK5CYII=" /> 
</svg>
);