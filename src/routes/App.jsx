import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// Pages
import Landing from '../pages/Landing';
import NotFound from '../pages/NotFound';
import Pokemons from '../pages/Pokemons';
import Categories from '../pages/Categories';
import Form from '../pages/Form';
import PokemonCreated from '../pages/PokemonCreated';
import Create from '../pages/Create';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Landing} />
        <Route exact path="/Pokedex101_Frontend" component={Landing} />
        <Route exact path="/pokemons" component={Pokemons} />
        <Route exact path="/pokemons/:category" component={Pokemons} />
        <Route exact path="/categories" component={Categories} />
        <Route exact path="/create" component={Create} />
        <Route exact path="/create/surprise" component={Form} />
        <Route exact path="/created" component={PokemonCreated} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  )
}

export default App;