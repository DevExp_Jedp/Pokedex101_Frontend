const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index.jsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js'
  },
  devServer: {
    historyApiFallback: true,
  },
  resolve: {
    extensions: [".jsx", ".js", ".sass", ".scss"]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /Node_modules/,
        use:[
          {
            loader: 'graphql-tag/loader'
          }
        ]
      },
      {
        test: /\.(s[ac]ss|css)/,
        exclude: /node_modules/,
        use: [
          MiniCSSExtractPlugin.loader,
          {
            loader: 'css-loader'
          },
          {
            loader: 'resolve-url-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              config: {
                path: 'postcss.config.js'
              }
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpeg|webp|gif|jpg)/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          filename: '[name].[ext]',
          publicPath: '/assets/images/',
          outputPath: 'assets/images/'
        } 
      },
      {
        test: /\.(mp4)/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          filename: '[name].[ext]',
          publicPath: '/assets/videos/',
          outputPath: 'assets/videos/'
        } 
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      favicon: './public/favicon.png',
      template: './public/index.html',
      filename: 'index.html'
    }),
    new MiniCSSExtractPlugin({
      filename: 'styles/[name].css',
      chunkFilename: 'styles/[id].css'
    })
  ]
}